//
//  ViewController.m
//  PilotPlanDemo
//
//  Created by Elena Timofeeva on 4/15/14.
//  Copyright (c) 2014 PilotPlanDemo. All rights reserved.
//

#import "LoginViewController.h"
#import "User.h"
#import "AppDelegate.h"
#import "FlyListViewController.h"

@interface LoginViewController ()

@property (nonatomic, strong) IBOutlet UITextField* loginField;
@property (nonatomic, strong) IBOutlet UITextField* passwordField;

@property (nonatomic, strong) NSArray* users;

@end


@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (((AppDelegate*)[UIApplication sharedApplication].delegate).currentUser.ID != 0)
    {
        FlyListViewController* flyListVC = [[UIStoryboard storyboardWithName: @"Main" bundle: nil] instantiateViewControllerWithIdentifier: @"FlyListViewController"];
        NSArray* viewControllers = @[flyListVC];
        self.navigationController.viewControllers = viewControllers;
    }
    else
    {
        User* user = [[User alloc] init];
        user.ID = 1;
        user.firstName = @"Jonah";
        user.lastName = @"Williams";
        user.userName = @"jwilliams";
        user.password = @"123456";
        
        User* user1 = [[User alloc] init];
        user1.ID = 2;
        user1.firstName = @"Roly";
        user1.lastName = @"Mottershead";
        user1.userName = @"rmottershead";
        user1.password = @"123456";
        
        User* user2 = [[User alloc] init];
        user2.ID = 3;
        user2.firstName = @"Darius";
        user2.lastName = @"Kay";
        user2.userName = @"dkay";
        user2.password = @"123456";
        
        User* user3 = [[User alloc] init];
        user3.ID = 5;
        user3.firstName = @"Lawrie";
        user3.lastName = @"Endicott";
        user3.userName = @"lendicott";
        user3.password = @"123456";
        
        User* user4 = [[User alloc] init];
        user4.ID = 6;
        user4.firstName = @"Linden";
        user4.lastName = @"Osbourne";
        user4.userName = @"losbourne";
        user4.password = @"123456";
        
        self.users = @[user, user1, user2, user3, user4];
    }
}


- (void) viewWillAppear: (BOOL) animated
{
    
    self.navigationController.navigationBarHidden = YES;
    
    [super viewWillAppear: animated];
}


- (BOOL) checkLoginPassword
{
    BOOL result = NO;
    
    for (User* user in self.users)
    {
        if ([user.userName isEqualToString: self.loginField.text] && [user.password isEqualToString: self.passwordField.text])
        {
            ((AppDelegate*)[UIApplication sharedApplication].delegate).currentUser = user;
            
            result = YES;
            break;
        }
    }
    
    return result;
}


- (BOOL) textFieldShouldReturn: (UITextField*) textField
{
    if (textField == self.loginField)
    {
        [self.loginField resignFirstResponder];
        [self.passwordField becomeFirstResponder];
    }
    else
    {
        if ([self checkLoginPassword])
        {
            [self performSegueWithIdentifier: @"FllyListSegue" sender: self];
        }
        else
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"" message: @"Invalid credentials" delegate: nil cancelButtonTitle: @"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    
    return NO;
}


@end
