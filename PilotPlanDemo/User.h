//
//  User.h
//  PilotPlanDemo
//
//  Created by Elena Timofeeva on 4/16/14.
//  Copyright (c) 2014 PilotPlanDemo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* userName;
@property (nonatomic, strong) NSString* password;

@end
