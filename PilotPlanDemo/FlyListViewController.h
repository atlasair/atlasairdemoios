//
//  FlyListViewController.h
//  PilotPlanDemo
//
//  Created by Elena Timofeeva on 4/16/14.
//  Copyright (c) 2014 PilotPlanDemo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KMStompJMS/KMStompJMS.h>
#import <KGWebSocket/WebSocket.h>


@interface FlyListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, KMStompConnectionListener, KMExceptionListener>

@property (atomic, strong) NSMutableDictionary* flies;
@property (nonatomic, strong) IBOutlet UITableView* tableView;

@end
