//
//  FlyTableViewCell.h
//  PilotPlanDemo
//
//  Created by Elena Timofeeva on 4/16/14.
//  Copyright (c) 2014 PilotPlanDemo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlyTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView* alertIndicator;
@property (nonatomic, strong) IBOutlet UILabel* flyCode;
@property (nonatomic, strong) IBOutlet UILabel* departureTimeLabel;
@property (nonatomic, strong) IBOutlet UILabel* departureAirport;
@property (nonatomic, strong) IBOutlet UILabel* arrivalTimeLabel;
@property (nonatomic, strong) IBOutlet UILabel* arrivalAirport;

@end
