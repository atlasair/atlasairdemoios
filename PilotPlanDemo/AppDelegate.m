//
//  AppDelegate.m
//  PilotPlanDemo
//
//  Created by Elena Timofeeva on 4/15/14.
//  Copyright (c) 2014 PilotPlanDemo. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate
{
    User* currentUser;
    NSData* devToken;
}

@dynamic currentUser;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName: @"BackgroundNotification" object: nil userInfo: nil];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName: @"ForegroundNotification" object: nil userInfo: nil];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] postNotificationName: @"BackgroundNotification" object: nil userInfo: nil];
}


- (void) setCurrentUser: (User*) user
{
    currentUser = user;
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    if (currentUser != nil)
    {
        [userDefaults setObject: currentUser.firstName forKey: @"firstName"];
        [userDefaults setObject: currentUser.lastName forKey: @"lastName"];
        [userDefaults setObject: currentUser.userName forKey: @"userName"];
        [userDefaults setObject: currentUser.password forKey: @"password"];
        [userDefaults setInteger: currentUser.ID forKey: @"ID"];
    }
    else
    {
        [userDefaults removeObjectForKey: @"firstName"];
        [userDefaults removeObjectForKey: @"lastName"];
        [userDefaults removeObjectForKey: @"userName"];
        [userDefaults removeObjectForKey: @"password"];
        [userDefaults removeObjectForKey: @"ID"];
    }
    
    [userDefaults synchronize];
}


- (User*) currentUser
{
    if (currentUser == nil)
    {
        currentUser = [[User alloc] init];
        
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        currentUser.firstName = [userDefaults objectForKey: @"firstName"];
        currentUser.lastName = [userDefaults objectForKey: @"lastName"];
        currentUser.userName = [userDefaults objectForKey: @"userName"];
        currentUser.password = [userDefaults objectForKey: @"password"];
        currentUser.ID = [userDefaults integerForKey: @"ID"];
    }
    
    NSLog(@"%@", currentUser);
    
    return currentUser;
}


/* Method for when the client registers for remote notifications
 with a device token */
- (void) application: (UIApplication*) app didRegisterForRemoteNotificationsWithDeviceToken: (NSData*) theDevToken
{
    NSLog(@"Successful registration: devToken = '%@'", [theDevToken description]);
    devToken = theDevToken;
}

/* Method for when the client fails to register for remote notifications.
 Outputs an error */
- (void) application: (UIApplication*) app didFailToRegisterForRemoteNotificationsWithError: (NSError*) err
{
    NSLog(@"Error in registration. Error: %@", err);
}

/* Method for when the client receives a remote notification.
 Outputs a message indicating receipt. */
- (void) application: (UIApplication*) application didReceiveRemoteNotification: (NSDictionary*) userInfo
{
    NSLog(@"Push notification arrived: %@", userInfo);
    NSString* alertString = [[userInfo objectForKey: @"aps"] objectForKey: @"alert"];
    
    if (alertString.length != 0)
    {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle: @"" message: alertString delegate: self cancelButtonTitle: NSLocalizedString(@"OK", @"") otherButtonTitles: nil];
        //[alertView show];
    }
}

// Get the iOS device token to return to the notification provider.
- (NSData*) deviceToken
{
    return devToken;
}


@end
