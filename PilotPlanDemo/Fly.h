//
//  Fly.h
//  PilotPlanDemo
//
//  Created by Elena Timofeeva on 4/18/14.
//  Copyright (c) 2014 PilotPlanDemo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fly : NSObject

@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, strong) NSString* flightNumber;
@property (nonatomic, strong) NSString* departureAirport;
@property (nonatomic, strong) NSString* arrivalAirport;
@property (nonatomic, strong) NSDate* departureDate;
@property (nonatomic, strong) NSDate* arrivalDate;
@property (nonatomic, assign) NSInteger departureHours;
@property (nonatomic, assign) NSInteger departureMinutes;
@property (nonatomic, strong) NSString* departureTimeShort;
@property (nonatomic, strong) NSString* arrivalTimeShort;
@property (nonatomic, assign) BOOL isNew;

@end
