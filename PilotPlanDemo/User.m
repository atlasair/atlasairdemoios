//
//  User.m
//  PilotPlanDemo
//
//  Created by Elena Timofeeva on 4/16/14.
//  Copyright (c) 2014 PilotPlanDemo. All rights reserved.
//

#import "User.h"

@implementation User

- (NSString*) description
{
    return [NSString stringWithFormat: @"ID: %d First name: %@ Last name: %@ User name: %@ Password: %@", self.ID, self.firstName, self.lastName, self.userName, self.password];
}

@end
