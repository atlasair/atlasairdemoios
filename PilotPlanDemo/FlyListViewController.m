//
//  FlyListViewController.m
//  PilotPlanDemo
//
//  Created by Elena Timofeeva on 4/16/14.
//  Copyright (c) 2014 PilotPlanDemo. All rights reserved.
//

#import "FlyListViewController.h"
#import "LoginViewController.h"
#import "FlyTableViewCell.h"
#import "AppDelegate.h"

#import "Fly.h"

@interface FlyListViewController ()

@end


@interface KaazingLoginHandler : KGLoginHandler
@end


@implementation KaazingLoginHandler

- (NSURLCredential*) credentials
{
    return [[NSURLCredential alloc] initWithUser: @"joe" password: @"welcome" persistence: NSURLCredentialPersistenceNone];
}

@end


@interface ListenersUtil : NSObject

+ (Fly*) initializeFlyWithDictionary: (NSDictionary*) dictionary;
+ (void) addOrReplaceFly: (Fly*) fly toArray: (NSMutableArray*) fliesArray;
+ (NSArray*) sortedDayFligths: (NSArray*) source;
+ (NSMutableDictionary*) sortedFlights: (NSArray*) allFlights;

@end


@implementation ListenersUtil

+ (Fly*) initializeFlyWithDictionary: (NSDictionary*) dictionary
{
    Fly* fly = [[Fly alloc] init];
    fly.ID = [[dictionary objectForKey: @"id"] integerValue];
    fly.flightNumber = [dictionary objectForKey: @"flightNumber"];
    fly.departureAirport = [dictionary objectForKey: @"departureCode"];
    fly.arrivalAirport = [dictionary objectForKey: @"arrivalCode"];
    fly.departureTimeShort = [dictionary objectForKey: @"departureTimeShort"];
    fly.arrivalTimeShort = [dictionary objectForKey: @"arrivalTimeShort"];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"yyyy-MM-dd"];
    
    fly.departureDate = [dateFormatter dateFromString: [[dictionary objectForKey: @"departureTime"] substringToIndex: 10]];
    fly.arrivalDate = [dateFormatter dateFromString: [[dictionary objectForKey: @"arrivalTime"] substringToIndex: 10]];
    
    NSLog(@"Fly code: %@, departure date: %@, arrival date: %@", fly.flightNumber, fly.departureDate, fly.arrivalDate);
    
    if (fly.departureDate == nil)
    {
        fly.departureDate = [NSDate new];
    }
    
    if (fly.arrivalDate == nil)
    {
        fly.arrivalDate = [NSDate new];
    }
    
    
    fly.departureHours = [[[dictionary objectForKey: @"departureTimeShort"] substringToIndex: 2] integerValue];
    fly.departureMinutes = [[[dictionary objectForKey: @"departureTimeShort"] substringWithRange: NSMakeRange(3, 2)] integerValue];
    
    NSLog(@"Fly code: %@, departure hours: %d, departure minutes: %d", fly.flightNumber, fly.departureHours, fly.departureMinutes);
    
    return fly;
}


+ (void) addOrReplaceFly: (Fly*) fly toArray: (NSMutableArray*) fliesArray
{
    BOOL exist = NO;
    for (int i = 0; i < [fliesArray count]; i++)
    {
        Fly* theFly = [fliesArray objectAtIndex: i];
        if (fly.ID == theFly.ID)
        {
            exist = YES;
            [fliesArray removeObjectAtIndex: i];
            [fliesArray insertObject: fly atIndex: i];
        }
    }
    
    if (!exist)
    {
        [fliesArray addObject: fly];
    }
}


+ (NSArray*) sortedDayFligths: (NSArray*) source
{
    NSArray* resultArray = [source sortedArrayUsingComparator: ^NSComparisonResult(id obj1, id obj2)
    {
        NSInteger firstHours = [(Fly*)obj1 departureHours];
        NSInteger firstMinutes = [(Fly*)obj1 departureMinutes];
        
        NSInteger secondHours = [(Fly*)obj2 departureHours];
        NSInteger secondMinutes = [(Fly*)obj2 departureMinutes];
        
        NSComparisonResult result;
        
        if (firstHours < secondHours)
        {
            result = NSOrderedAscending;
        }
        else if (firstHours > secondHours)
        {
            result = NSOrderedDescending;
        }
        else if (firstMinutes < secondMinutes)
        {
            result = NSOrderedAscending;
        }
        else if (firstMinutes > secondMinutes)
        {
            result = NSOrderedDescending;
        }
        else
        {
            result = NSOrderedSame;
        }
        
        return result;
    }];
    
    return resultArray;
}


+ (NSMutableDictionary*) sortedFlights: (NSArray*) allFlights
{
    NSArray* sortedFliesArray = [allFlights sortedArrayUsingComparator: ^ NSComparisonResult(id a, id b)
                                 {
                                     NSDate *first = [(Fly*)a departureDate];
                                     NSDate *second = [(Fly*)b departureDate];
                                     return [first compare:second];
                                 }];
    
    NSMutableDictionary* finalDict = [[NSMutableDictionary alloc] initWithCapacity: 0];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    int i = 0;
    
    while (i < [sortedFliesArray count])
    {
        NSMutableArray* dayFlightsArray = [[NSMutableArray alloc] initWithCapacity: 0];
        
        [dayFlightsArray addObject: [sortedFliesArray objectAtIndex: i]];
        NSDate* firstDate = ((Fly*)[sortedFliesArray objectAtIndex: i]).departureDate;
        
        while (YES)
        {
            i++;
            
            if (i >= [sortedFliesArray count])
            {
                break;
            }
            
            NSDateComponents* firstDateComponents = [gregorian components: NSMonthCalendarUnit | NSDayCalendarUnit fromDate: firstDate];
            NSDateComponents* secondDateComponents = [gregorian components: NSMonthCalendarUnit | NSDayCalendarUnit fromDate: ((Fly*)[sortedFliesArray objectAtIndex: i]).departureDate];
            
            if (firstDateComponents.day == secondDateComponents.day && firstDateComponents.month == secondDateComponents.month)
            {
                [dayFlightsArray addObject: [sortedFliesArray objectAtIndex: i]];
            }
            else
            {
                break;
            }
        }
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat: @"MMMM dd, yyyy"];
        NSString* dateString = [dateFormatter stringFromDate: ((Fly*)[dayFlightsArray objectAtIndex: 0]).departureDate];
        
        if (dateString != nil)
        {
            [finalDict setObject: [ListenersUtil sortedDayFligths: dayFlightsArray] forKey: dateString];
        }
    }
    
    return finalDict;
}


@end




@interface AllFliesMessageListener : NSObject <KMMessageListener>
@end

@implementation AllFliesMessageListener
{
    FlyListViewController* flyVC;
}


- (id) initWithFlyVC: (FlyListViewController*) theFlyVC
{
    self = [super init];
    
    if (self != nil)
    {
        flyVC = theFlyVC;
    }
    
    return self;
}


- (void) onMessage: (KMMessage*) message
{
    if ([message isKindOfClass:[KMTextMessage class]])
    {
        KMTextMessage *textMessage = (KMTextMessage *)message;
        NSLog(@"RECEIVED KMTextMessage: %@", [textMessage text]);
        
        NSError* error = nil;
        NSData* jsonData = [[textMessage text] dataUsingEncoding: NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData: jsonData options: kNilOptions error: &error];
        
        BOOL isDict = NO;
        
        if ([json respondsToSelector: @selector(objectForKey:)])
        {
            isDict = YES;
        }
        
        if ([json count] != 0 && error == nil)
        {
            NSMutableArray* fliesArray = [[NSMutableArray alloc] initWithCapacity: [json count]];
            
            if (!isDict)
            {
                for (NSDictionary* flyDict in json)
                {
                    [fliesArray addObject: [ListenersUtil initializeFlyWithDictionary: flyDict]];
                }
            }
            else
            {
                [fliesArray addObject: [ListenersUtil initializeFlyWithDictionary: json]];
            }
            
            NSMutableDictionary* finalDict = [ListenersUtil sortedFlights: fliesArray];
            
            dispatch_async(dispatch_get_main_queue(), ^
            {
                flyVC.flies = finalDict;
            });
        }
    }
    else if ([message isKindOfClass:[KMBytesMessage class]])
    {
        KMBytesMessage *bytesMessage = (KMBytesMessage *)message;
        NSString *utf8String = [bytesMessage readUTF];
        NSLog(@"RECEIVED KMBytesMessage: %@", utf8String);
        
    }
    else if ([message isKindOfClass:[KMMapMessage class]])
    {
        KMMapMessage *mapMessage = (KMMapMessage *)message;
        NSLog(@"RECEIVED KMMapMessage:");
        
        int count = 0;
        for (NSString *key in [mapMessage mapNames]) {
            id value = [mapMessage getObject:key];
            NSString *type = NSStringFromClass([value class]);
            NSLog(@"  %@: %@ (%@)", key, value, type);
            count++;
        }
        NSLog(@"%d entries", count);
        
    }
    else
    {
        NSLog(@"RECEIVED UNKNOWN MESSAGE");
    }
}

@end



@interface AddFlyMessageListener : NSObject <KMMessageListener>
@end

@implementation AddFlyMessageListener
{
    FlyListViewController* flyVC;
}


- (id) initWithFlyVC: (FlyListViewController*) theFlyVC
{
    self = [super init];
    
    if (self != nil)
    {
        flyVC = theFlyVC;
    }
    
    return self;
}


- (void) onMessage: (KMMessage*) message
{
    if ([message isKindOfClass:[KMTextMessage class]])
    {
        KMTextMessage *textMessage = (KMTextMessage *)message;
        NSLog(@"RECEIVED KMTextMessage: %@", [textMessage text]);
        
        NSError* error = nil;
        NSData* jsonData = [[textMessage text] dataUsingEncoding: NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData: jsonData options: kNilOptions error: &error];
        
        BOOL isDict = NO;
        
        if ([json respondsToSelector: @selector(objectForKey:)])
        {
            isDict = YES;
        }
        
        if ([json count] != 0 && error == nil)
        {
            NSArray* allKeys = [flyVC.flies allKeys];
            NSMutableArray* fliesArray = [[NSMutableArray alloc] initWithCapacity: [json count] + [allKeys count]];
            
            for (NSString* key in allKeys)
            {
                NSArray* flies = [flyVC.flies objectForKey: key];
                for (Fly* fly in flies)
                {
                    [fliesArray addObject: fly];
                }
            }
            
            if (!isDict)
            {
                for (NSDictionary* flyDict in json)
                {
                    Fly* fly = [ListenersUtil initializeFlyWithDictionary: flyDict];
                    fly.isNew = YES;
                    
                    [ListenersUtil addOrReplaceFly: fly toArray: fliesArray];
                }
            }
            else
            {
                Fly* fly = [ListenersUtil initializeFlyWithDictionary: json];
                fly.isNew = YES;
                
                [ListenersUtil addOrReplaceFly: fly toArray: fliesArray];
            }
            
            NSMutableDictionary* finalDict = [ListenersUtil sortedFlights: fliesArray];
            
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               flyVC.flies = finalDict;
                           });
        }
    }
    else if ([message isKindOfClass:[KMBytesMessage class]])
    {
        KMBytesMessage *bytesMessage = (KMBytesMessage *)message;
        NSString *utf8String = [bytesMessage readUTF];
        NSLog(@"RECEIVED KMBytesMessage: %@", utf8String);
        
    }
    else if ([message isKindOfClass:[KMMapMessage class]])
    {
        KMMapMessage *mapMessage = (KMMapMessage *)message;
        NSLog(@"RECEIVED KMMapMessage:");
        
        int count = 0;
        for (NSString *key in [mapMessage mapNames]) {
            id value = [mapMessage getObject:key];
            NSString *type = NSStringFromClass([value class]);
            NSLog(@"  %@: %@ (%@)", key, value, type);
            count++;
        }
        NSLog(@"%d entries", count);
        
    }
    else
    {
        NSLog(@"RECEIVED UNKNOWN MESSAGE");
    }
}

@end


@interface EditFlyMessageListener : NSObject <KMMessageListener>
@end

@implementation EditFlyMessageListener
{
    FlyListViewController* flyVC;
}


- (id) initWithFlyVC: (FlyListViewController*) theFlyVC
{
    self = [super init];
    
    if (self != nil)
    {
        flyVC = theFlyVC;
    }
    
    return self;
}


- (void) onMessage: (KMMessage*) message
{
    if ([message isKindOfClass:[KMTextMessage class]])
    {
        KMTextMessage *textMessage = (KMTextMessage *)message;
        NSLog(@"RECEIVED KMTextMessage: %@", [textMessage text]);
        
        NSError* error = nil;
        NSData* jsonData = [[textMessage text] dataUsingEncoding: NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData: jsonData options: kNilOptions error: &error];
        
        BOOL isDict = NO;
        
        if ([json respondsToSelector: @selector(objectForKey:)])
        {
            isDict = YES;
        }
        
        if ([json count] != 0 && error == nil)
        {
            NSArray* allKeys = [flyVC.flies allKeys];
            NSMutableArray* fliesArray = [[NSMutableArray alloc] initWithCapacity: [json count] + [allKeys count]];
            
            for (NSString* key in allKeys)
            {
                NSArray* flies = [flyVC.flies objectForKey: key];
                for (Fly* fly in flies)
                {
                    [fliesArray addObject: fly];
                }
            }
            
            if (!isDict)
            {
                for (NSDictionary* flyDict in json)
                {
                    Fly* fly = [ListenersUtil initializeFlyWithDictionary: flyDict];
                    fly.isNew = YES;
                    
                    [ListenersUtil addOrReplaceFly: fly toArray: fliesArray];
                }
            }
            else
            {
                Fly* fly = [ListenersUtil initializeFlyWithDictionary: json];
                fly.isNew = YES;
                
                [ListenersUtil addOrReplaceFly: fly toArray: fliesArray];
            }
            
            NSMutableDictionary* finalDict = [ListenersUtil sortedFlights: fliesArray];
            
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               flyVC.flies = finalDict;
                           });
        }
    }
    else if ([message isKindOfClass:[KMBytesMessage class]])
    {
        KMBytesMessage *bytesMessage = (KMBytesMessage *)message;
        NSString *utf8String = [bytesMessage readUTF];
        NSLog(@"RECEIVED KMBytesMessage: %@", utf8String);
        
    }
    else if ([message isKindOfClass:[KMMapMessage class]])
    {
        KMMapMessage *mapMessage = (KMMapMessage *)message;
        NSLog(@"RECEIVED KMMapMessage:");
        
        int count = 0;
        for (NSString *key in [mapMessage mapNames]) {
            id value = [mapMessage getObject:key];
            NSString *type = NSStringFromClass([value class]);
            NSLog(@"  %@: %@ (%@)", key, value, type);
            count++;
        }
        NSLog(@"%d entries", count);
        
    }
    else
    {
        NSLog(@"RECEIVED UNKNOWN MESSAGE");
    }
}

@end


@interface DeleteFlyMessageListener : NSObject <KMMessageListener>
@end

@implementation DeleteFlyMessageListener
{
    FlyListViewController* flyVC;
}


- (id) initWithFlyVC: (FlyListViewController*) theFlyVC
{
    self = [super init];
    
    if (self != nil)
    {
        flyVC = theFlyVC;
    }
    
    return self;
}


- (void) onMessage: (KMMessage*) message
{
    if ([message isKindOfClass:[KMTextMessage class]])
    {
        KMTextMessage *textMessage = (KMTextMessage *)message;
        NSLog(@"RECEIVED KMTextMessage: %@", [textMessage text]);
        
        NSArray* allKeys = [flyVC.flies allKeys];
        NSMutableArray* fliesArray = [[NSMutableArray alloc] initWithCapacity: [allKeys count]];
        
        for (NSString* key in allKeys)
        {
            NSArray* flies = [flyVC.flies objectForKey: key];
            for (Fly* fly in flies)
            {
                [fliesArray addObject: fly];
            }
        }
        
        for (int i = 0; i < [fliesArray count]; i++)
        {
            Fly* theFly = [fliesArray objectAtIndex: i];
            
            if ([[textMessage text] integerValue] == theFly.ID)
            {
                [fliesArray removeObjectAtIndex: i];
                break;
            }
        }

        NSMutableDictionary* finalDict = [ListenersUtil sortedFlights: fliesArray];
        
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           flyVC.flies = finalDict;
                       });
    }
    else if ([message isKindOfClass:[KMBytesMessage class]])
    {
        KMBytesMessage *bytesMessage = (KMBytesMessage *)message;
        NSString *utf8String = [bytesMessage readUTF];
        NSLog(@"RECEIVED KMBytesMessage: %@", utf8String);
        
    }
    else if ([message isKindOfClass:[KMMapMessage class]])
    {
        KMMapMessage *mapMessage = (KMMapMessage *)message;
        NSLog(@"RECEIVED KMMapMessage:");
        
        int count = 0;
        for (NSString *key in [mapMessage mapNames]) {
            id value = [mapMessage getObject:key];
            NSString *type = NSStringFromClass([value class]);
            NSLog(@"  %@: %@ (%@)", key, value, type);
            count++;
        }
        NSLog(@"%d entries", count);
        
    }
    else
    {
        NSLog(@"RECEIVED UNKNOWN MESSAGE");
    }
}

@end



@implementation FlyListViewController
{
    BOOL connected;
    KMConnection* conn;
    KMSession* session;
    KMNotifyingSession* notifyingSession;
    
    __strong NSMutableDictionary* flies;
}


@dynamic flies;


- (void) viewDidLoad
{
    [super viewDidLoad];
    
    NSArray* viewControllers = @[self];
    self.navigationController.viewControllers = viewControllers;
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(backgroundNotificationReceived:) name: @"BackgroundNotification" object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(foregroundNotificationReceived:) name: @"ForegroundNotification" object: nil];
}


- (void) viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    
    [super viewDidUnload];
}


- (void) viewWillAppear: (BOOL) animated
{
    self.navigationController.navigationBarHidden = NO;
    
    [self connectOrDisconnect];
    
    [super viewWillAppear: animated];
}


- (IBAction) logout: (id) sender
{
    [self updateStatus: NO logout: YES];
}


- (void) backgroundNotificationReceived: (NSNotification*) notification
{
    [self updateStatus: NO logout: NO];
}


- (void) foregroundNotificationReceived: (NSNotification*) notification
{
    [conn start];
    [self updateStatus: YES logout: NO];
}


- (void) setFlies: (NSMutableDictionary*) theFlies
{
    flies = theFlies;
    
    if ([flies count] != 0)
    {
        self.tableView.hidden = NO;
        [self.tableView reloadData];
    }
    else
    {
        self.tableView.hidden = YES;
    }
}


- (NSMutableDictionary*) flies
{
    return flies;
}


#pragma mark - KMStompConnectionListener


- (void) onConnect:(KMConnection *)connection
{
    connected = YES;
    NSLog(@"CONNECTED");
}

- (void) onStart:(KMConnection *)connection
{
    NSLog(@"STARTED");
    
    [self updateStatus: YES logout: NO];
    
    dispatch_async(dispatch_get_main_queue(), ^
                   {
                       [self getAllFlies];
                       [self performSelector: @selector(subscribeToAdd) withObject: nil afterDelay: 2];
                       [self performSelector: @selector(subscribeToEdit) withObject: nil afterDelay: 3];
                       [self performSelector: @selector(subscribeToDelete) withObject: nil afterDelay: 4];
                   });
}

- (void) onStop:(KMConnection *)connection
{
    NSLog(@"STOPPED");
}

- (void) onClose:(KMConnection *)connection
{
    connected = NO;
    NSLog(@"CLOSED");
}


#pragma mark - KMExceptionListener


- (void) onException: (KMJMSException*) exception
{
    NSString *msg = [exception reason];
    NSLog(@"EXCEPTION HANDLER: %@", msg);
    
    if ([msg rangeOfString: @"reconnected"].location != NSNotFound)
    {
        [self onStart: conn];
    }
}


#pragma mark - Connection and messaging


- (KGChallengeHandler *) createBasicChallengeHandler {
    // Set up ChallengeHandlers to handle authentication challenge.
    KGLoginHandler          *loginHandler = [[KaazingLoginHandler alloc] init];
    KGBasicChallengeHandler *challengeHandler = [KGBasicChallengeHandler create];
    
    [challengeHandler setLoginHandler:loginHandler];
    return challengeHandler;
}


- (void) connectOrDisconnect
{
    NSString *location = /*@"ws://192.168.176.44:8001/jms";*/ @"ws://91.234.37.134:8001/jms";
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (!connected)
        {
            NSURL                       *url = [[NSURL alloc] initWithString:location];
            KMStompConnectionProperties *props = [[KMStompConnectionProperties alloc] init];
            
            props->_connectionTimeout = 1000000;
            
            @try
            {
                KMStompConnectionFactory *factory = [[KMStompConnectionFactory alloc] initWithUrl:url];
                KGWebSocketFactory       *webSocketFactory = [factory webSocketFactory];
                KGChallengeHandler       *challengeHandler = [self createBasicChallengeHandler];
                
                [webSocketFactory setDefaultChallengeHandler: challengeHandler];
                
                NSLog(@"CONNECTING");
                
                id<KMStompConnectionListener> listener = self;
                conn = [factory createConnectionWithListener: listener];
                [conn setExceptionListener: self];
                [conn start];
                session = (KMSession *)[conn createSession: KMSessionAutoAcknowledge transacted: NO];
            }
            @catch (NSException *ex)
            {
                NSLog(@"Exception: %@", [ex reason]);
            }
        }
        else
        {
            connected = NO;
            NSLog(@"CLOSING");
            [conn close];
            conn = nil;
        }
    });
}


- (void) getAllFlies
{
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    
    if (appDelegate.currentUser != nil && connected)
    {
        // Subscribing can be a blocking operation, so perform it in the background.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @try {
                sleep(1);
                
                // Create a destination.
                KMTopic* topic = [session createTopic: @"/topic/FlightPlan.Read"];
                KMMessageProducer* producer = [session createProducer: topic];
                
                KMTemporaryQueue* temporaryQueue = [session createTemporaryQueue];
                
                KMMessage* message = [session createMessage];
                [message setIntProperty: @"pilotId" value: appDelegate.currentUser.ID];
                message.JMSReplyTo = temporaryQueue;
                
                // Create a consumer and attach a listener.
                KMMessageConsumer* consumer = [session createConsumer: temporaryQueue];
                [consumer setMessageListener: [[AllFliesMessageListener alloc] initWithFlyVC: self]];
                
                [producer send: message];
                
                NSLog(@"Getting all flights");
            }
            @catch (NSException *exception) {
                NSString *msg = [exception reason];
                NSLog(@"EXCEPTION: %@", msg);
            }
        });
    }
}


- (void) subscribeToAdd
{
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    
    if (appDelegate.currentUser != nil && connected)
    {
        // Subscribing can be a blocking operation, so perform it in the background.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @try {
                sleep(1);
                
                // Create a destination.
                KMQueue* queue = [session createQueue: [NSString stringWithFormat: @"/queue/FlightPlan.Add.%d", appDelegate.currentUser.ID]];
                
                // Create a consumer and attach a listener.
                KMMessageConsumer* consumer = [session createConsumer: queue];
                [consumer setMessageListener: [[AddFlyMessageListener alloc] initWithFlyVC: self]];
                
                NSLog(@"Subscribing to add");
            }
            @catch (NSException *exception) {
                NSString *msg = [exception reason];
                NSLog(@"EXCEPTION: %@", msg);
            }
        });
    }
}


- (void) subscribeToEdit
{
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    
    if (appDelegate.currentUser != nil && connected)
    {
        // Subscribing can be a blocking operation, so perform it in the background.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @try {
                sleep(1);
                
                // Create a destination.
                KMQueue* queue = [session createQueue: [NSString stringWithFormat: @"/queue/FlightPlan.Update.%d", appDelegate.currentUser.ID]];
                
                // Create a consumer and attach a listener.
                KMMessageConsumer* consumer = [session createConsumer: queue];
                [consumer setMessageListener: [[EditFlyMessageListener alloc] initWithFlyVC: self]];
                
                NSLog(@"Subscribing to edit");
            }
            @catch (NSException *exception) {
                NSString *msg = [exception reason];
                NSLog(@"EXCEPTION: %@", msg);
            }
        });
    }
}


- (void) subscribeToDelete
{
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    
    if (appDelegate.currentUser != nil && connected)
    {
        // Subscribing can be a blocking operation, so perform it in the background.
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @try {
                sleep(1);
                
                // Create a destination.
                KMQueue* queue = [session createQueue: [NSString stringWithFormat: @"/queue/FlightPlan.Delete.%d", appDelegate.currentUser.ID]];
                
                // Create a consumer and attach a listener.
                KMMessageConsumer* consumer = [session createConsumer: queue];
                [consumer setMessageListener: [[DeleteFlyMessageListener alloc] initWithFlyVC: self]];
                
                NSLog(@"Subscribing to edit");
            }
            @catch (NSException *exception) {
                NSString *msg = [exception reason];
                NSLog(@"EXCEPTION: %@", msg);
            }
        });
    }
}


- (void) updateStatus: (BOOL) online logout: (BOOL) logout
{
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    
    if (appDelegate.currentUser != nil && connected)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @try {
                if (online)
                {
                    sleep(1);
                }
                
                KMQueue* queue = [session createQueue: @"/queue/Pilot.Status"];
                KMMessageProducer* producer = [session createProducer: queue];
                
                NSString* deviceToken = [[[appDelegate.deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
                
                NSString* messageText = [NSString stringWithFormat: @"{id:%d, isOnline:%@, devToken:\"%@\", devManufactor:\"apple\", logout:%@}", appDelegate.currentUser.ID, ((online) ? @"true" : @"false"), (deviceToken == nil) ? @"" : deviceToken, (logout ? @"true" : @"false")];
                
                NSLog(@"Status message: %@", messageText);
                
                KMMessage* message = [session createTextMessage: messageText];
                
                [producer send: message];
                
                if (logout)
                {
                    dispatch_async(dispatch_get_main_queue(),  ^{
                        ((AppDelegate*)[UIApplication sharedApplication].delegate).currentUser = nil;
                        
                        LoginViewController* loginVC = [[UIStoryboard storyboardWithName: @"Main" bundle: nil] instantiateViewControllerWithIdentifier: @"LoginViewController"];
                        NSArray* viewControllers = @[loginVC, self];
                        self.navigationController.viewControllers = viewControllers;
                        
                        [self.navigationController popViewControllerAnimated: YES];
                    });
                }
                
                NSLog(@"Status sent");
            }
            @catch (NSException *exception) {
                NSString *msg = [exception reason];
                NSLog(@"EXCEPTION: %@", msg);
            }
        });
    }
}


- (void) sendAcknowlege: (NSInteger) flyID
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @try
        {
            KMQueue* queue = [session createQueue: @"/queue/FlightPlan.Acknowledge"];
            KMMessageProducer* producer = [session createProducer: queue];
            
            NSString* messageText = [NSString stringWithFormat: @"%d", flyID];
            NSLog(@"Set acknowlage message: %@", messageText);
            KMMessage* message = [session createTextMessage: messageText];
            
            [producer send: message];
        }
        @catch (NSException *exception) {
            NSString *msg = [exception reason];
            NSLog(@"EXCEPTION: %@", msg);
        }
    });
}



#pragma mark -


- (NSArray*) sortedDateKeys
{
    NSArray* sortedArray = [[flies allKeys] sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat: @"MMMM dd, yyyy"];
        NSDate* date1 = [dateFormatter dateFromString: (NSString*)obj1];
        NSDate* date2 = [dateFormatter dateFromString: (NSString*)obj2];
        return [date1 compare: date2];
    }];
    
    return sortedArray;
}



#pragma mark - Table view data source


- (NSInteger) numberOfSectionsInTableView: (UITableView*) tableView
{
    // Return the number of sections.
    NSInteger sections = [[self sortedDateKeys] count];
    return sections;
}


- (NSInteger) tableView: (UITableView*) tableView numberOfRowsInSection: (NSInteger) section
{
    NSArray* keys = [self sortedDateKeys];
    NSInteger rows = [[flies objectForKey: [keys objectAtIndex: section]] count];
    return rows;
}


- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSArray* keys = [self sortedDateKeys];
    NSString* title = [keys objectAtIndex: section];
    return title;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray* keys = [self sortedDateKeys];
    Fly* fly = [[flies objectForKey: [keys objectAtIndex: indexPath.section]] objectAtIndex: indexPath.row];
    FlyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"FlyCell" forIndexPath: indexPath];
    
    cell.flyCode.text = fly.flightNumber;
    cell.departureAirport.text = fly.departureAirport;
    cell.departureTimeLabel.text = fly.departureTimeShort;
    cell.arrivalAirport.text = fly.arrivalAirport;
    cell.arrivalTimeLabel.text = fly.arrivalTimeShort;
    cell.alertIndicator.hidden = !fly.isNew;

    return cell;
}


#pragma mark - Table View Delegate


- (void) tableView: (UITableView*) tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath
{
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    
    FlyTableViewCell* cell = (FlyTableViewCell*)[tableView cellForRowAtIndexPath: indexPath];
    cell.alertIndicator.hidden = YES;
    
    NSArray* keys = [self sortedDateKeys];
    Fly* fly = [[flies objectForKey: [keys objectAtIndex: indexPath.section]] objectAtIndex: indexPath.row];
    fly.isNew = NO;
    [self sendAcknowlege: fly.ID];
}


@end
