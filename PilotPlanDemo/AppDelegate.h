//
//  AppDelegate.h
//  PilotPlanDemo
//
//  Created by Elena Timofeeva on 4/15/14.
//  Copyright (c) 2014 PilotPlanDemo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) User* currentUser;

- (NSData*) deviceToken;

@end
